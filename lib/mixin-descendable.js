/**
 *    parent/child : refers to a contains relationship.  A parent contains children.
 *    ancestor/descendant : refers to a prototypal relationship.  Descendants are created from (have as their prototype) ancestors.
 * TODO: consider separating container/containable into separate mixin which detects descendable.
 */

module.exports = ({self, parent, ancestor, childTypes}) =>
{
	if (!self)
		throw Error('required parameter "self" missing.');

	let target = Object.create(Descendable);

	let proxy = new Proxy(target, {get : GET, set : SET});
	
	target.self      = self;
	target.proxy     = proxy;
	target.parent    = parent;
	target.ancestor  = ancestor;

	target.childMap   = Object.create(null);
	target.childList  = [];
	target.childTypes = childTypes;

	return proxy;
};

const Descendable =
{
	self     : undefined, //un-proxied underlying object
	proxy    : undefined, //current proxied object
	parent   : undefined, //parent object, proxied
	ancestor : undefined, //ancestor proxy object

	childMap   : undefined, //children indexed by their ID
	childList  : undefined, //list of child IDs, used to keep the sorting correct.
	childTypes : undefined, //list of prototypes allowed for children.  Leave blank to allow any. 

	/**
	 * Adds a child to this container.  Must be descendable.
	 * @param {string} id 
	 * @param {descendable} child 
	 */
	addChild (id, child)
	{
		let {self, childList, childMap, ancestor, childTypes} = this;

		if (child.isDescendable !== BUOY)
			throw Error('Only other descendable objects can be added as children.');

		if (childTypes && childTypes.reduce((carry, current) => p.isPrototypeOf(child) || carry))
			throw Error('Can\'t add child with id "' + id + '", not in allowed class list.');

		if (childList.includes(id) || ancestor.includes(id))
			throw Error('Child with id " + id + " already exists on this object or its ancestor.');

		if (FORBIDDENIDS.includes(id) || self[id])
			throw Error('ID "' + id + '" is reserved and cannot be used.');

		childList.push(id);
		childMap[id] = child.descend(self);
		return this;
	},

	/**
	 * Adds a child to this container.
	 * @param {string} id 
	 */
	hasChild (id)
	{
		let {childList, ancestor} = this;

		return childList.includes(id) || ancestor.hasChild(id);
	},

	/**
	 * Remove a child from this container.  Only works on the original item a 
	 * child was added to, and propagates to descendants.
	 * @param {string} id 
	 */
	removeChild (id)
	{
		let {self, childList, childMap, ancestor} = this;

		childMap[id] = undefined;
		self.childList = childList.splice(childList.indexOf(id));
		return this;
	},

	/**
	 * Overrides a child at the ID of its parent.
	 * @param {string} id name of the item to add.
	 * @param {descendable} object the object to add.  Set it to false to disable that child entirely.
	 */
	overrideChild (id, object)
	{
		let {childList, childMap, ancestor} = this;

		if (object && object.isDescendable !== BUOY)
			throw Error('Only other descendable objects can be added as children.');
		
		if (!childList.includes(id) || !ancestor.includes(id))
			throw Error('No child with id " + id + " exists to override.');
		
		childMap[id] = object;
		return this;
	},
	
	/**
	 * Instantiates the object as a new proxy with a new parent.
	 */
	descend (parent)
	{
		return module.exports(
		{
			self       : Object.create(this.self),
			parent     : parent,
			ancestor   : this.proxy,
			childTypes : this.childTypes,
		});
	},

 	/**
	 * Gets all the children of the object in an iterable form.  As an intended
	 * side-effect, it also descends all of them with the current object as
	 * parent.
	 */
	get children ()
	{
		let {proxy, ancestor} = this;
		[...ancestor.childlist, ...childList].map(k=>proxy[k])
	},

	/**
	 * A kludge to allow isPrototypeOf with multiple inheritance. 
	 */
	isPrototypeOf (object)
	{
		console.log('ipo');
		if (object && this.self)
			return super.isPrototypeOf(object.proxy) || this.self.isPrototypeOf(object) || object.self && this.self.isPrototypeOf(object.self);
		return super.isPrototypeOf(object);
	},
};

const GET = (target, key) =>
{
	if (key === 'isPrototypeOf')
		return target.isPrototypeOf;

	if (target.ancestor && target.ancestor[key] && target.ancestor[key].isDescendable)
		return target.ancestor[key];

	if (target.childMap[key])
		return target.childMap[key];

	if (target.self[key])
		return target.self[key];

	return target[key];
};

const SET = (target, key, value) =>
{
	if (target.childMap[key] || target.ancestor && target.ancestor[key] && target.ancestor[key].isDescendable)
		throw Error('Use "addChild", "removeChild", and "overrideChild" methods to manage child objects.');

	target.self[key] = value;
};
