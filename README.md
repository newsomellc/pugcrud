# pugcrud

A CRUD system built in pug.  Populates from various sources automatically.  Like Sails, but less complete.  And less bloated.

DOES NOT and WILL NOT handle authentication.  TODO: Post express recipes to show how to handle auth in middleware.

## Structure
* app:
	* server-side support
		* web sockets
		* automatic REST service (automatice)
	* exclusively server-side
	* intended to be included by external projects
* cli:
	* cli commands
	* exclusively server-side
	* not for third-party use
	* contains standalone server and configurator
* client:
	* js:
		* client side browser scripts
		* exclusively client side.
	* sass:
		* client-side (obviously) styles.
		* Bootstrap compatible (for now, may expand to non-bootstrap later)
* dist:
	* pre-compiled
		* CSS
		* JS
	* minified version
	* debug symbol version
	* for people who don't use Browserify/Sass
	* Maybe it should be able to work exclusively client-side as well?
* model:
	* code to manage structure of CRUD system
	* code to render HTML
	* usable both server and client-side
	* templates:
		* Actual pug templates here
		* obviously, client AND server side
		* (consider) Overridable?
* test:
	* our very complete test suite
