/**
 * A schema is a container for models.  It's used to group them and set up their
 * hierarchy and relationships.
 *
 * A schema can contain either other schemas or collections.
 * CONSIDER: Make all objects proxies, auth functions instantiated on property access?  That way, you just return a variant of a schema with the auth flag set for the base parent, then when you ask for its children they get instantiated on demand.
 */
let DESCENDABLE = require('../lib/mixin-descendable');

const Schema = 
{
	authorize (allowedFN)
	{
		let descendant = this.descend();
		descendant.allowed = allowedFN;
		return descendant;
	},

	allowed (...permissions)
	{
		return process.env.NODE_ENV !== 'production';
	},

	
};

module.exports = DESCENDABLE({self : Schema, childTypes: [Schema, require('./collection'),]});
