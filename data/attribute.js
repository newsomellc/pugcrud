/**
 * An attribute describes a property on an entity.  It represents a column in a 
 * database, or an attribute on a file, or anything else.
 */