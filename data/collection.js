/**
 * A collection is the main "listing" for data entities.  It corresponds to a 
 * database table and its display on screen.
 */
