const Model = require('../src')();

let model = Model.construct(
{
	id : 'test-crud',
	name : 'Crud for TESTING.',
});

model.addField(
{
	id : 'name',
	type : 'text',
	path : 'name',

});

let form = model.Form.construct();

form.addChild('name');

console.log(form.render());
